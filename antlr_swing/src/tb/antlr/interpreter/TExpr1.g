tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (print | expr)*;

print   : (PRINT_KEYWORD e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
        //| (PRINT_KEYWORD e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
       //| (PRINT_KEYWORD e=comparisonExpression) {drukuj ($e.text + " = " + $e.out.toString());}
        ;
      
//logicalExpression returns [Boolean out]
//        : ^(AND e1=logicalExpression e2=logicalExpression) {$out =$e1.out && $e2.out;}
//        | ^(OR e1=logicalExpression e2=logicalExpression) {$out =$e1.out || $e2.out;}
//        | ^(AND e1=comparisonExpression e2=comparisonExpression) {$out =$e1.out && $e2.out;}
//        | ^(OR e1=comparisonExpression e2=comparisonExpression) {$out =$e1.out || $e2.out;}
////        | LITERAL_FALSE   {$out = getBoolean($LITERAL_FALSE.text);}
////        | LITERAL_TRUE    {$out = getBoolean($LITERAL_TRUE.text);}
//        ;    

//comparisonExpression returns [Boolean out]
//        : ^(EQ e1=expr e2=expr) {$out =$e1.out == $e2.out;}
//        | ^(NEQ e1=expr e2=expr) {$out =$e1.out != $e2.out;}
//        | ^(GTE e1=expr e2=expr) {$out =$e1.out >= $e2.out;}
//        | ^(LTE e1=expr e2=expr) {$out =$e1.out <= $e2.out;}
//        | ^(GT e1=expr e2=expr) {$out =$e1.out > $e2.out;}
//        | ^(LT e1=expr e2=expr) {$out =$e1.out < $e2.out;}       
//        ;
  
expr returns [Double out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(POW   e1=expr e2=expr) {$out = Math.pow($e1.out, $e2.out);}
        | ^(ASSIGN i1=IDENTIFIER   e2=expr)
        | INTEGER                      {$out = getDouble($INTEGER.text);}
        ;

